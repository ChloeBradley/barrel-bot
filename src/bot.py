import discord 
import asyncio
from string import punctuation
import os
from random import randint
import datetime

barrel_ims = []
for path, subdirs, files in os.walk('barrels/'):
	for name in files:
		if '.ini' not in name:
			barrel_ims.append(os.path.join(path, name))

with open('token.txt') as f:
	token = f.read().replace('\r\n', '')

last_time = datetime.datetime.now()
command_list = []
with open('commands.txt') as f:
	for line in f:
		line = line.rstrip().split("\t")
		command_list.append(line)

print(command_list)

client = discord.Client()

@client.event
async def barrel_command(message):
	now = datetime.datetime.now()
	if (now-last_time).total_seconds() > 1: #(60*10): # 10 minutes
		#await client.send_message(message.channel, '<:puni_jiro:305614974707957761>')
		i = randint(0,len(barrel_ims)-1)
		await client.send_file(message.channel, barrel_ims[i])
		global last_time
		last_time = now
	return

@client.event
async def image_command(i, message):
	await client.send_file(message.channel, barrel_ims[i])
	await client.send_message(message.channel, command_list[i,2])
	return

@client.event
async def command(i, whole_message, message):
	if command_list[i][1].strip() == 'barrel':
		await barrel_command(message)
	elif command_list[i][1].strip() == 'im' and whole_message:
		await image_command(i, message);
	else:
		print('Command error:')
		print(command_list[i])
		return

@client.event
async def on_ready():
	print('Logged in as')
	print(client.user.name)
	print(client.user.id)
	print('------')

@client.event
async def on_message(message):
	for i in range(0, len(command_list)):
		word = command_list[i][0].strip().lower()
		if word in message.content.lower().strip():
			if word is message.content.lower().strip():
				await command(i, True, message)
			else:
				await command(i, False, message)
			break
		i += 1



client.run(token)
